/////////////////////////////////////////////////////////////////////////////////
//
//	MainActivity.java
//	Unity Android HID Multiplayer Sample Project
//	� 2013 Bensussen Deutsch and Associates, Inc. All rights reserved.
//
//	description:	Shows how to pair Controllers using HID on Android.
//
/////////////////////////////////////////////////////////////////////////////////

package com.bda.examplehid;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.example.inputmanagercompat.InputManagerCompat;
import com.example.inputmanagercompat.InputManagerCompat.InputDeviceListener;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.util.SparseArray;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class MainActivity extends Activity implements InputDeviceListener
{
	private SparseArray<ExamplePlayer> playersArray;
	private InputManagerCompat mInputManager;
	GLSurfaceView mView = null;
	
	/***
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Create our View
		mView = new GLSurfaceView(this);
		mView.setRenderer(new ExampleRenderer());
		mView.setKeepScreenOn(true);
		((FrameLayout)findViewById(R.id.myframelayout)).addView(mView, 0);
		
		// Create our Players Array
		playersArray = new SparseArray<ExamplePlayer>();
		
		// Create our Input Manager and Set the Listener
		mInputManager = InputManagerCompat.Factory.getInputManager(this.getBaseContext());
		mInputManager.registerInputDeviceListener(this, null);
	}

	/***
	 * 
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Application has entered the background
	 */
	protected void OnPause()
	{
		// Stop polling for disconnected devices
		mInputManager.onPause();
	}
	
	/**
	 * Application has returned to the Foreground
	 */
	protected void OnResume()
	{
		// Start polling for disconnected devices
		mInputManager.onResume();
	}
	
	// Connection Management --------------------------------------------------------------------
	
	/**
	 * When a Controller is Connected, Link with Player
	 */
	@Override
	public void onInputDeviceAdded(int deviceId) 
	{
		GetPlayerForDeviceId(deviceId);
	}
	
	/**
	 * If a Controller has Changed, get the current linked Player
	 * and update the previously linked Controller with this one.
	 */
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onInputDeviceChanged(int deviceId) 
	{
		ExamplePlayer player = GetPlayerForDeviceId(deviceId);
		player.setInputDevice(InputDevice.getDevice(deviceId));
	}

	/**
	 * Remove the Player when their Controller was disconnected.
	 */
	@Override
	public void onInputDeviceRemoved(int deviceId) 
	{
		playersArray.remove(deviceId);
	}
	
	// Input Handling --------------------------------------------------------------------------
	
	@Override
	/**
	 * Handle Button Down Events
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		// Get the Device ID...
		int deviceId = event.getDeviceId();
		
		// If the Device ID is available...
		if (deviceId != -1)
		{
			// Check whether this device is already linked to a player.
			ExamplePlayer currentPlayer = GetPlayerForDeviceId(deviceId);
			
			// If it is, send the linked Player a KeyEvent to handle.
			if (currentPlayer.onKeyDown(keyCode, event))
			{
				return true;
			}
		}
		
		return super.onKeyDown(keyCode, event);
	}
   
    /***
     * Handle Button Released Events
     */
	 @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) 
    {
		// Get the Device ID...
        int deviceId = event.getDeviceId();
        
        // If the Device ID is available...
        if (deviceId != -1) 
        {
        	// Check whether this device is already linked to a player.
            ExamplePlayer currentPlayer = GetPlayerForDeviceId(deviceId);
            
            // If it is, send the linked Player a KeyEvent to handle.
            if (currentPlayer.onKeyUp(keyCode, event)) 
            {
                return true;
            }
        }

        return super.onKeyUp(keyCode, event);
    }
	
	/***
	 * Handle Axis Events (JoySticks and Triggers)
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	@Override
	public boolean onGenericMotionEvent(MotionEvent event)
	{
		mInputManager.onGenericMotionEvent(event);
		
		// Get the Device Type
		int eventSource = event.getSource();
		
		// If the Device is a Gamepad or Joystick
		if ((((eventSource & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD)
			|| ((eventSource & InputDevice.SOURCE_CLASS_JOYSTICK) == InputDevice.SOURCE_CLASS_JOYSTICK)) && event.getAction() == MotionEvent.ACTION_MOVE)
		{
			// Get the Device ID...
			int id = event.getDeviceId();
			
			// If the Device ID is available...
			if (id != -1)
			{
				// Get the assoicated Player attached to this device..
				ExamplePlayer currentPlayer = GetPlayerForDeviceId(id);
				
				// If the player exists, send the motion event...
				if (currentPlayer.onGenericMotionEvent(event))
				{
					return true;
				}
			}
		}
		
		return super.onGenericMotionEvent(event);
	}
	
	// Player ---------------------------------------------------------------------
	
	/**
	 * Returns the Player thats linked with the Controller, or Creates
	 * a new Player a new controller was connected.
	 * @param deviceID
	 * @return ExamplePlayer
	 */
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private ExamplePlayer GetPlayerForDeviceId(int deviceId)
	{
		// Check to see if a Player linked to this Controller already exists...
		ExamplePlayer currentPlayer = playersArray.get(deviceId);
		
		// If not, create one and link it with this Controller...
		if (currentPlayer == null)
		{
			InputDevice  dev = InputDevice.getDevice(deviceId);
			currentPlayer = new ExamplePlayer();
			playersArray.append(deviceId, currentPlayer);
			currentPlayer.setInputDevice(dev);
		}
		
		return currentPlayer;
	}
	
	 /***
     * Creates a Player
     * @author BDA
     *
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	class ExamplePlayer
	{        
    	@SuppressWarnings("unused")
		private InputDevice mInputDevice;
    	
        private float 	horizontal,
        				vertical,
        				lookHorizontal,
        				lookVertical;
        
        private float	xPos,
        				yPos;

		public ExamplePlayer()
		{	                  

		}
		
		/**
		 * Handle Button Released Events
		 * @param keyCode
		 * @param event
		 * @return
		 */
		public boolean onKeyUp(int keyCode, KeyEvent event) 
		{
            boolean handled = false;
            
            switch (keyCode) 
            {
                case KeyEvent.KEYCODE_BUTTON_A:
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_BUTTON_START:
                    handled = true;
                    break;
            }
            
            return handled;
		}

		/**
		 * 
		 * @param event
		 * @return
		 */
		public boolean onGenericMotionEvent(MotionEvent event) 
		{
        	// Process all historical movement samples in the batch.
            final int historySize = event.getHistorySize();
                
            for (int i = 0; i < historySize; i++) 
            {
            	horizontal 		= event.getAxisValue(MotionEvent.AXIS_X);
                vertical 		= event.getAxisValue(MotionEvent.AXIS_Y);
                lookHorizontal 	= event.getAxisValue(MotionEvent.AXIS_Z);
                lookVertical 	= event.getAxisValue(MotionEvent.AXIS_RZ);
            }

            // Process the current movement sample in the batch.
        	horizontal 		= event.getAxisValue(MotionEvent.AXIS_X);
            vertical 		= event.getAxisValue(MotionEvent.AXIS_Y);
            lookHorizontal 	= event.getAxisValue(MotionEvent.AXIS_Z);
            lookVertical 	= event.getAxisValue(MotionEvent.AXIS_RZ);

            return true;
		}

		/**
		 * Handle Button Pressed Events
		 * @param keyCode
		 * @param event
		 * @return
		 */
		public boolean onKeyDown(int keyCode, KeyEvent event) 
		{
			boolean handled = false;
			
			if (event.getRepeatCount() == 0)
			{
				switch (keyCode)
				{
				case KeyEvent.KEYCODE_BUTTON_A:
					handled = true;
					break;
				case KeyEvent.KEYCODE_BUTTON_B:
					handled = true;
					break;
				}
			}
			return handled;
		}

		/**
		 * Set the Controller that is attached to this Player.
		 * @param dev
		 */
		public void setInputDevice(InputDevice dev)
		{
			mInputDevice = dev;
		}
	}
    
    // Renderer ---------------------------------------------------------------------
    
	class ExampleRenderer implements GLSurfaceView.Renderer
	{
		final FloatBuffer mVertexBuffer;

		public ExampleRenderer()
		{
			final float[] vertices = {5.0f, 0.0f, 0.0f, 0.0f, 5.0f, 0.0f, -5.0f, 0.0f, 0.0f, 0.0f, -5.0f, 0.0f};
			final ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
			byteBuffer.order(ByteOrder.nativeOrder());
			mVertexBuffer = byteBuffer.asFloatBuffer();
			mVertexBuffer.put(vertices);
			mVertexBuffer.position(0);
		}

		@Override
		public void onDrawFrame(GL10 gl) {

			gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			gl.glMatrixMode(GL10.GL_MODELVIEW);

			int key = 0;
			for(int i = 0; i < playersArray.size(); i++) 
			{
			   key = playersArray.keyAt(i);
			   final ExamplePlayer player = playersArray.get(key);
				
			   final float scale = 10.0f;
				player.xPos += (player.horizontal + player.lookHorizontal) * scale;
				player.yPos -= (player.vertical + player.lookVertical) * scale;

				gl.glLoadIdentity();
				gl.glTranslatef(player.xPos, player.yPos, 0.0f);
				gl.glScalef(10.0f, 10.0f, 10.0f);
				gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
				gl.glColor4f(1f, 0f, 0f, 1f);
				gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer);
					
				gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, 4);
			}
				
			gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
			
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {

			gl.glViewport(0, 0, width, height);
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
			gl.glOrthof(width / -2.0f, width / +2.0f, height / -2.0f, height / +2.0f, -1.0f, 1.0f);
			
		}

		@Override
		public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
			// TODO Auto-generated method stub
			
		}
	}
}
