/////////////////////////////////////////////////////////////////////////////////
//
//	MainActivity.java
//	Unity Android HID Multiplayer Sample Project
//	� 2013 Bensussen Deutsch and Associates, Inc. All rights reserved.
//
//	description:	Shows how to pair Controllers using HID on Android.
//					Empty Tutorial Starting Project.
//
/////////////////////////////////////////////////////////////////////////////////

package com.bda.examplehidempty;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.util.SparseArray;
import android.view.Menu;
import android.widget.FrameLayout;

public class MainActivity extends Activity
{
	private SparseArray<ExamplePlayer> playersArray;
	
	GLSurfaceView mView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mView = new GLSurfaceView(this);
		mView.setRenderer(new ExampleRenderer());
		mView.setKeepScreenOn(true);
		((FrameLayout)findViewById(R.id.myframelayout)).addView(mView, 0);
		
		playersArray = new SparseArray<ExamplePlayer>();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	protected void OnPause()
	{

	}
	
	protected void OnResume()
	{

	}
	
	// Player ---------------------------------------------------------------------
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	class ExamplePlayer
	{        
        private float 	horizontal,
        				vertical,
        				lookHorizontal,
        				lookVertical;
        
        private float	xPos,
        				yPos;

		public ExamplePlayer()
		{	                  

		}
	}
    
    // Renderer ---------------------------------------------------------------------
    
	class ExampleRenderer implements GLSurfaceView.Renderer
	{
		final FloatBuffer mVertexBuffer;

		public ExampleRenderer()
		{
			final float[] vertices = {5.0f, 0.0f, 0.0f, 0.0f, 5.0f, 0.0f, -5.0f, 0.0f, 0.0f, 0.0f, -5.0f, 0.0f};
			final ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
			byteBuffer.order(ByteOrder.nativeOrder());
			mVertexBuffer = byteBuffer.asFloatBuffer();
			mVertexBuffer.put(vertices);
			mVertexBuffer.position(0);
		}

		@Override
		public void onDrawFrame(GL10 gl) {

			gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			gl.glMatrixMode(GL10.GL_MODELVIEW);

			int key = 0;
			for(int i = 0; i < playersArray.size(); i++) 
			{
			   key = playersArray.keyAt(i);
			   final ExamplePlayer player = playersArray.get(key);
				
			   final float scale = 10.0f;
				player.xPos += (player.horizontal + player.lookHorizontal) * scale;
				player.yPos -= (player.vertical + player.lookVertical) * scale;

				gl.glLoadIdentity();
				gl.glTranslatef(player.xPos, player.yPos, 0.0f);
				gl.glScalef(10.0f, 10.0f, 10.0f);
				gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
				gl.glColor4f(1f, 0f, 0f, 1f);
				gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer);
					
				gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, 4);
			}
				
			gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
			
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {

			gl.glViewport(0, 0, width, height);
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
			gl.glOrthof(width / -2.0f, width / +2.0f, height / -2.0f, height / +2.0f, -1.0f, 1.0f);
			
		}

		@Override
		public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
			// TODO Auto-generated method stub
			
		}
	}
}
